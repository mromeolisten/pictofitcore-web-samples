This repository contains sample setups for the Pictofit Web Viewer

Note: The pictofit.min.js file is expected to be placed next to the index.html file.

Currently contains sample setups for the following use cases:

* untextured mesh
* textured mesh
* multiple resources
* config list for model switching